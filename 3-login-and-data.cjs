// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


const fs = require("fs")
const util = require("util")

const writeFile = util.promisify(fs.writeFile)
const unlink = util.promisify(fs.unlink)
const path = require("path")

function WriteFile(file) {
    return new Promise((resolve, reject) => {
        writeFile(path.join(__dirname, `${file}.json`), JSON.stringify({ 'key': 'Data' }), (error) => {
            if (error) {
                new Error("error", error)
                reject(error)
            } else {
                console.log("Created")
                resolve()
            }
        })

    })
}
function DeletFiles(array) {
    return new Promise((resolve, reject) => {
        array.map((element)=>{
            unlink(path.join(__dirname, `${element}.json`), (error) => {
                if (error) {
                    error = new Error("error in deleting")
                    reject(error)
                } else {
                    console.log("deleted")
                    resolve()
                }
            })
        })
        
    })
}
function CreatedFiles(firstfolder, secondfolder) {
    let arr = [firstfolder, secondfolder]
    let filesOne = WriteFile(`${firstfolder}`)
    let fileTwo = WriteFile(`${secondfolder}`)
    let deletefolder = new Promise((resolve, reject) => {
        setTimeout(() => {
            return DeletFiles(arr)

        }, 2 * 1000)
    })
    return Promise.all([filesOne, fileTwo, deletefolder]).then(() => {
        console.log("files created")

    })

}

CreatedFiles("file", "filesecond")

